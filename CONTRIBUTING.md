# Contributing
Any contribution is very welcome.  

## How to contribute?
Create your Pull Request, it will be reviewed by other Android Developers; once is approved you can merge it 
into master and that's all.

## PR Template
Every time you create a PR we recommend using this template.
```
## Description
A short description giving an idea of what is the code about.

## Screenshots (if apply)  

```