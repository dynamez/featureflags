package cl.scotiabank.featureflags.data.remote.launchdarkly

import cl.scotiabank.commons.testingtools.RandomFactory
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.LaunchDarklyClient
import com.launchdarkly.sdk.LDValue
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class LaunchDarklyRemoteImplTest {

    private val launchDarklyClient = mockk<LaunchDarklyClient>()
    private val remote = LaunchDarklyRemoteImpl(launchDarklyClient)

    @Test
    fun `given map, when getFlags, then return data`() = runBlocking {
        val randomMap = RandomFactory.generateMapOfStrings(5)
        stubClientGetAllFlags(randomMap)

        val resultFlow = remote.getFlags()

        resultFlow.collect { result ->
            assertEquals(randomMap, result, "randomMap")
        }
    }

    private fun stubClientGetAllFlags(randomMap: Map<String, String>) {
        coEvery { launchDarklyClient.getAllFlags() } returns randomMap
    }

    @Test
    fun `given boolean, when getBooleanFlagByKey, then return data`() = runBlocking {
        val randomBoolean = RandomFactory.generateBoolean()
        stubClientGetBooleanFlagByKey(randomBoolean)

        val resultFlow = remote.getBooleanFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomBoolean, result, "randomBoolean")
        }
    }

    private fun stubClientGetBooleanFlagByKey(randomBoolean: Boolean) {
        coEvery { launchDarklyClient.getBooleanFlag(any()) } returns randomBoolean
    }

    @Test
    fun `given string, when getStringFlagByKey, then return data`() = runBlocking {
        val randomString = RandomFactory.generateString()
        stubClientGetStringFlagByKey(randomString)

        val resultFlow = remote.getStringFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomString, result, "randomString")
        }
    }

    private fun stubClientGetStringFlagByKey(randomString: String) {
        coEvery { launchDarklyClient.getStringFlag(any()) } returns randomString
    }

    @Test
    fun `given int, when getIntFlagByKey, then return data`() = runBlocking {
        val randomInt = RandomFactory.generateInt()
        stubClientGetIntFlagByKey(randomInt)

        val resultFlow = remote.getIntFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomInt, result, "randomInt")
        }
    }

    private fun stubClientGetIntFlagByKey(randomInt: Int) {
        coEvery { launchDarklyClient.getIntFlag(any()) } returns randomInt
    }

    @Test
    fun `given float, when getDoubleFlagByKey, then return data`() = runBlocking {
        val randomDouble = RandomFactory.generateDouble()
        stubClientGetDoubleFlagByKey(randomDouble)

        val result = remote.getDoubleFlagByKey(RandomFactory.generateString()).toList().first()

        assertEquals(randomDouble, result)
    }

    private fun stubClientGetDoubleFlagByKey(randomDouble: Double) {
        coEvery { launchDarklyClient.getDoubleFlag(any()) } returns randomDouble
    }

    @Test
    fun `given json, when getLDValueFlagByKey, then return data`() = runBlocking {
        val randomLDValue: LDValue = LDValue
            .buildObject()
            .put("key", RandomFactory.generateString())
            .build()
        stubClientGetLDValueFlagByKey(randomLDValue)

        val result = remote.getLDValueFlagByKey(RandomFactory.generateString()).toList().first()

        assertEquals(randomLDValue, result)
    }

    private fun stubClientGetLDValueFlagByKey(ldValue: LDValue) {
        coEvery { launchDarklyClient.getLDValueFlag(any()) } returns ldValue
    }

}