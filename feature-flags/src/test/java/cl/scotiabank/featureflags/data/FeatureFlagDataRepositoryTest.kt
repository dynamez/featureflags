package cl.scotiabank.featureflags.data

import cl.scotiabank.commons.testingtools.RandomFactory
import cl.scotiabank.featureflags.data.source.Remote
import com.launchdarkly.sdk.LDValue
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class FeatureFlagDataRepositoryTest {

    private val remote = mockk<Remote>()
    private val repository = FeatureFlagDataRepository(remote)

    @Test
    fun `given random map, when getAllFeatures, then return data`() = runBlocking {
        val randomMap = RandomFactory.generateMapOfStrings(10)
        stubRemoteGetFeatures(randomMap)

        val resultFlow = repository.getFlags()

        resultFlow.collect { result ->
            assertEquals(randomMap, result, "randomMap")
        }
    }

    private fun stubRemoteGetFeatures(randomMap: Map<String, String>) {
        coEvery { remote.getFlags() } returns flow { emit(randomMap) }
    }

    @Test
    fun `given random boolean, when getBooleanFeatureByKey, then return data`() = runBlocking {
        val randomBoolean = RandomFactory.generateBoolean()
        stubRemoteGetBooleanFeatureByKey(randomBoolean)

        val resultFlow = repository.getBooleanFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomBoolean, result, "randomBoolean")
        }
    }

    private fun stubRemoteGetBooleanFeatureByKey(randomBoolean: Boolean) {
        coEvery { remote.getBooleanFlagByKey(any()) } returns flow { emit(randomBoolean) }
    }

    @Test
    fun `given random int, when getStringFeatureByKey, then return data`() = runBlocking {
        val randomString = RandomFactory.generateString()
        stubRemoteGetStringFeatureByKey(randomString)

        val resultFlow = repository.getStringFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomString, result, "randomString")
        }
    }

    private fun stubRemoteGetStringFeatureByKey(randomString: String) {
        coEvery { remote.getStringFlagByKey(any()) } returns flow { emit(randomString) }
    }

    @Test
    fun `given random int, when getIntFeatureByKey, then return data`() = runBlocking {
        val randomInt = RandomFactory.generateInt()
        stubRemoteGetIntFeatureByKey(randomInt)

        val resultFlow = repository.getIntFlagByKey(RandomFactory.generateString())

        resultFlow.collect { result ->
            assertEquals(randomInt, result, "randomInt")
        }
    }

    private fun stubRemoteGetIntFeatureByKey(randomInt: Int) {
        coEvery { remote.getIntFlagByKey(any()) } returns flow { emit(randomInt) }
    }

    @Test
    fun `given random float, when getFloatFeatureByKey, then return data`() = runBlocking {
        val randomDouble = Random.nextDouble()
        stubRemoteGetFloatFeatureByKey(randomDouble)

        val result = repository.getDoubleFlagByKey(RandomFactory.generateString()).toList().first()

        assertEquals(randomDouble, result)
    }

    private fun stubRemoteGetFloatFeatureByKey(randomDouble: Double) {
        coEvery { remote.getDoubleFlagByKey(any()) } returns flow { emit(randomDouble) }
    }

    @Test
    fun `given random JsonElement, when getJsonFeatureByKey, then return data`() = runBlocking {
        val randomLDValue: LDValue = LDValue
            .buildObject()
            .put("key", RandomFactory.generateString())
            .build()
        stubRemoteGetLDValueByKey(randomLDValue)

        val result = repository.getLDValueFlagByKey(RandomFactory.generateString()).toList().first()

        assertEquals(randomLDValue, result)
    }

    private fun stubRemoteGetLDValueByKey(ldValue: LDValue) {
        coEvery { remote.getLDValueFlagByKey(any()) } returns flow { emit(ldValue) }
    }

}