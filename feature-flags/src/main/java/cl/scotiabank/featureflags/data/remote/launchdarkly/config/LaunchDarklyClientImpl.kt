package cl.scotiabank.featureflags.data.remote.launchdarkly.config

import android.app.Application
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.BACKGROUND_DISABLED
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DEFAULT_BOOLEAN
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DEFAULT_INT
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DEFAULT_STRING
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DOUBLE_FLOAT
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.OFFLINE
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.START_WAIT_SECONDS
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.defaultLDValue
import com.launchdarkly.sdk.LDUser
import com.launchdarkly.sdk.LDValue
import com.launchdarkly.sdk.android.ConnectionInformation
import com.launchdarkly.sdk.android.LDClient
import com.launchdarkly.sdk.android.LDConfig

class LaunchDarklyClientImpl(
    application: Application,
    ldConfig: LDConfig,
    ldUser: LDUser
) : LaunchDarklyClient {

    private var ldClient: LDClient? = null

    init {
        ldClient = LDClient.init(application, ldConfig, ldUser, START_WAIT_SECONDS) // FIXME START_WAIT_SECONDS should be a param?
    }

    override fun getAllFlags(): Map<String, *> = ldClient?.allFlags() ?: emptyMap<String, String>()

    override fun getBooleanFlag(flagKey: String): Boolean =
            ldClient?.boolVariation(flagKey, DEFAULT_BOOLEAN) ?: DEFAULT_BOOLEAN

    override fun getStringFlag(flagKey: String): String =
            ldClient?.stringVariation(flagKey, DEFAULT_STRING).orEmpty()

    override fun getIntFlag(flagKey: String): Int =
            ldClient?.intVariation(flagKey, DEFAULT_INT) ?: DEFAULT_INT

    override fun getDoubleFlag(flagKey: String): Double =
            ldClient?.doubleVariation(flagKey, DOUBLE_FLOAT) ?: DOUBLE_FLOAT

    override fun getLDValueFlag(flagKey: String): LDValue =
            ldClient?.jsonValueVariation(flagKey, defaultLDValue) ?: defaultLDValue

    override fun getConnectionInformation(): Boolean {
        val connectionInfo: ConnectionInformation = ldClient?.connectionInformation ?: return false
        return when(connectionInfo.connectionMode.toString()) {
            OFFLINE -> false
            BACKGROUND_DISABLED -> false
            else -> true
        }
    }
}
