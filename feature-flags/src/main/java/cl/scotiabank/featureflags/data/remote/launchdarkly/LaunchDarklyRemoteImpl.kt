package cl.scotiabank.featureflags.data.remote.launchdarkly

import cl.scotiabank.featureflags.data.remote.launchdarkly.config.LaunchDarklyClient
import cl.scotiabank.featureflags.data.source.Remote
import com.launchdarkly.sdk.LDValue
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class LaunchDarklyRemoteImpl(private val launchDarklyClient: LaunchDarklyClient) : Remote {

    override fun getFlags(): Flow<Map<String, *>> = flow {
        val allFlags = launchDarklyClient.getAllFlags()
        emit(allFlags)
    }

    override fun getBooleanFlagByKey(flagKey: String): Flow<Boolean> = flow {
        val booleanFlag = launchDarklyClient.getBooleanFlag(flagKey)
        emit(booleanFlag)
    }

    override fun getStringFlagByKey(flagKey: String): Flow<String> = flow {
        val stringFlag = launchDarklyClient.getStringFlag(flagKey)
        emit(stringFlag)
    }

    override fun getIntFlagByKey(flagKey: String): Flow<Int> = flow {
        val intFlag = launchDarklyClient.getIntFlag(flagKey)
        emit(intFlag)
    }

    override fun getDoubleFlagByKey(flagKey: String): Flow<Double> = flow {
        val floatFlag = launchDarklyClient.getDoubleFlag(flagKey)
        emit(floatFlag)
    }

    override fun getLDValueFlagByKey(flagKey: String): Flow<LDValue> = flow {
        val ldValueFlag = launchDarklyClient.getLDValueFlag(flagKey)
        emit(ldValueFlag)
    }
}
