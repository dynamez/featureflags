package cl.scotiabank.featureflags.data.source

import com.launchdarkly.sdk.LDValue
import kotlinx.coroutines.flow.Flow

interface Remote {

    fun getFlags(): Flow<Map<String, *>>

    fun getBooleanFlagByKey(flagKey: String): Flow<Boolean>

    fun getStringFlagByKey(flagKey: String): Flow<String>

    fun getIntFlagByKey(flagKey: String): Flow<Int>

    fun getDoubleFlagByKey(flagKey: String): Flow<Double>

    fun getLDValueFlagByKey(flagKey: String): Flow<LDValue>
}
