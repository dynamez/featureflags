package cl.scotiabank.featureflags.data.remote.launchdarkly.config

import com.launchdarkly.sdk.LDValue

interface LaunchDarklyClient {

    fun getAllFlags(): Map<String, *>

    fun getBooleanFlag(flagKey: String): Boolean

    fun getStringFlag(flagKey: String): String

    fun getIntFlag(flagKey: String): Int

    fun getDoubleFlag(flagKey: String): Double

    fun getLDValueFlag(flagKey: String): LDValue

    fun getConnectionInformation(): Boolean
}
