package cl.scotiabank.featureflags.data.remote.launchdarkly.config

import android.app.Application
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DEFAULT_INT
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DEFAULT_STRING
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.DOUBLE_FLOAT
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.Constants.defaultMap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.launchdarkly.sdk.LDValue
import java.lang.reflect.Type

class LaunchDarklyDummyClientImpl(
        application: Application,
        dummyFilePath: String
) : LaunchDarklyClient {

    private var dummyFileMap: Map<String, *>? = null

    init {
        val jsonString = application
            .assets
            .open(dummyFilePath).bufferedReader()
            .use { it.readText() }
        val type: Type = object : TypeToken<Map<String, *>>() {}.type
        dummyFileMap = Gson().fromJson(jsonString, type)
    }

    override fun getAllFlags(): Map<String, *> = dummyFileMap ?: defaultMap

    override fun getBooleanFlag(flagKey: String): Boolean = true

    override fun getStringFlag(flagKey: String): String = DEFAULT_STRING

    override fun getIntFlag(flagKey: String): Int = DEFAULT_INT

    override fun getDoubleFlag(flagKey: String): Double = DOUBLE_FLOAT

    override fun getLDValueFlag(flagKey: String): LDValue {
        val rawValue = dummyFileMap?.get(flagKey)
        return LDValue.parse(rawValue.toString())
    }

    override fun getConnectionInformation(): Boolean = true
}
