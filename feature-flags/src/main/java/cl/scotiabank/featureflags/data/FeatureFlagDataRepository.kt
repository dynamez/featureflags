package cl.scotiabank.featureflags.data

import cl.scotiabank.featureflags.data.source.Remote
import com.launchdarkly.sdk.LDValue
import kotlinx.coroutines.flow.Flow

class FeatureFlagDataRepository(private val remote: Remote) {

    fun getFlags(): Flow<Map<String, *>> = remote.getFlags()

    fun getBooleanFlagByKey(flagKey: String): Flow<Boolean> = remote.getBooleanFlagByKey(flagKey)

    fun getStringFlagByKey(flagKey: String): Flow<String> = remote.getStringFlagByKey(flagKey)

    fun getIntFlagByKey(flagKey: String): Flow<Int> = remote.getIntFlagByKey(flagKey)

    fun getDoubleFlagByKey(flagKey: String): Flow<Double> = remote.getDoubleFlagByKey(flagKey)

    fun getLDValueFlagByKey(flagKey: String): Flow<LDValue> = remote.getLDValueFlagByKey(flagKey)
}
