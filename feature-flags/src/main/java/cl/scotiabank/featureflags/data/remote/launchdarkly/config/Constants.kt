package cl.scotiabank.featureflags.data.remote.launchdarkly.config

import com.launchdarkly.sdk.LDValue

object Constants {

    const val START_WAIT_SECONDS = 5

    const val OFFLINE = "OFFLINE"
    const val BACKGROUND_DISABLED = "BACKGROUND_DISABLED"
    const val DEFAULT_BOOLEAN = false
    const val DEFAULT_STRING = ""
    const val DEFAULT_INT = 0
    const val DOUBLE_FLOAT = 0.0
    val defaultLDValue: LDValue = LDValue.buildArray().build()
    val defaultMap: Map<String, Any> = mapOf()
}