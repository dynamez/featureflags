package cl.scotiabank.featureflags.presentation.rawdata

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import cl.scotiabank.featureflags.presentation.di.Source
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn

class FlagByKeyViewModel : ViewModel() {

    fun booleanLiveData(featureKey: String, source: Source) =
        booleanStateFlow(featureKey, source).asLiveData()

    fun stringLiveData(featureKey: String, source: Source) =
        stringStateFlow(featureKey, source).asLiveData()

    fun intLiveData(featureKey: String, source: Source) =
        intStateFlow(featureKey, source).asLiveData()

    fun doubleLiveData(featureKey: String, source: Source) =
        doubleStateFlow(featureKey, source).asLiveData()

    fun ldLiveData(featureKey: String, source: Source) =
        ldValueStateFlow(featureKey, source).asLiveData()

    fun booleanStateFlow(featureKey: String, source: Source) = source
        .getFeatureFlagDataRepository()
        .getBooleanFlagByKey(featureKey)
        .flowOn(Dispatchers.IO)

    fun stringStateFlow(featureKey: String, source: Source) = source
        .getFeatureFlagDataRepository()
        .getStringFlagByKey(featureKey)
        .flowOn(Dispatchers.IO)

    fun intStateFlow(featureKey: String, source: Source) = source
        .getFeatureFlagDataRepository()
        .getIntFlagByKey(featureKey)
        .flowOn(Dispatchers.IO)

    fun doubleStateFlow(featureKey: String, source: Source) = source
        .getFeatureFlagDataRepository()
        .getDoubleFlagByKey(featureKey)
        .flowOn(Dispatchers.IO)

    fun ldValueStateFlow(featureKey: String, source: Source) = source
        .getFeatureFlagDataRepository()
        .getLDValueFlagByKey(featureKey)
        .flowOn(Dispatchers.IO)
}
