package cl.scotiabank.featureflags.presentation.custommodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import cl.scotiabank.featureflags.data.FeatureFlagDataRepository
import cl.scotiabank.featureflags.presentation.di.Source
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach

class AllFlagsViewModel<TFeature> : ViewModel() {

    private val stateFlow: MutableStateFlow<Map<String, TFeature>> = MutableStateFlow(mapOf())
    private var liveData: LiveData<Map<String, TFeature>> = MutableLiveData()
    var source: Source? = null

    fun getAllUsingKotlinFlow(tClass: Class<TFeature>) {
        requireNotNull(source) { "Source must be initialize. You can provide either RemoteSource or DummySource." }
        source?.let {
            getFeatures(
                    tClass = tClass,
                repository = it.getFeatureFlagDataRepository()
            ).onEach { features ->
                stateFlow.value = features
            }.launchIn(viewModelScope)
        }
    }

    fun getAllUsingLiveData(tClass: Class<TFeature>) {
        requireNotNull(source) { "Source must be initialize. You can provide either RemoteSource or DummySource." }
        source?.let {
            liveData = getFeatures(
                    tClass = tClass,
                repository = it.getFeatureFlagDataRepository()
            ).asLiveData()
        }
    }

    private fun getFeatures(
        tClass: Class<TFeature>,
        repository: FeatureFlagDataRepository
    ): Flow<Map<String, TFeature>> = repository
        .getFlags()
        .map { featuresMap ->
            featuresMap.mapNotNull { mapEntry ->
                parseFeaturesMap(tClass = tClass, mapEntry = mapEntry)
            }.toMap()
        }.catch {
            emit(mapOf())
        }.flowOn(Dispatchers.IO)

    private fun parseFeaturesMap(
        tClass: Class<TFeature>,
        mapEntry: Map.Entry<String, Any?>
    ): Pair<String, TFeature>? = try {
        val featureString = mapEntry.value.toString()
        val parsedFeature = Gson().fromJson(featureString, tClass)
        Pair(mapEntry.key, parsedFeature)
    } catch (e: Throwable) {
        null
    }

    fun stateFlow(): StateFlow<Map<String, TFeature>> = stateFlow

    fun liveData(): LiveData<Map<String, TFeature>> = liveData
}
