package cl.scotiabank.featureflags.presentation.di

import cl.scotiabank.featureflags.data.FeatureFlagDataRepository

interface Source {
    fun getFeatureFlagDataRepository(): FeatureFlagDataRepository
}
