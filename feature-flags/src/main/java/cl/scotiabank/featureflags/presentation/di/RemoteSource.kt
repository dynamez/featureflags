package cl.scotiabank.featureflags.presentation.di

import android.app.Application
import cl.scotiabank.featureflags.data.FeatureFlagDataRepository
import cl.scotiabank.featureflags.data.remote.launchdarkly.LaunchDarklyRemoteImpl
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.LaunchDarklyClientImpl
import com.launchdarkly.sdk.LDUser
import com.launchdarkly.sdk.android.LDConfig

class RemoteSource(
    application: Application,
    ldConfig: LDConfig,
    ldUser: LDUser
) : Source {

    private val launchDarklyClient = LaunchDarklyClientImpl(
            application = application,
            ldConfig = ldConfig,
            ldUser = ldUser
    )
    private val remote = LaunchDarklyRemoteImpl(launchDarklyClient)
    private val repository = FeatureFlagDataRepository(remote)
    override fun getFeatureFlagDataRepository(): FeatureFlagDataRepository = repository
}
