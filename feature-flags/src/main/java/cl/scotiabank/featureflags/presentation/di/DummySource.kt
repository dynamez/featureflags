package cl.scotiabank.featureflags.presentation.di

import android.app.Application
import cl.scotiabank.featureflags.data.FeatureFlagDataRepository
import cl.scotiabank.featureflags.data.remote.launchdarkly.LaunchDarklyRemoteImpl
import cl.scotiabank.featureflags.data.remote.launchdarkly.config.LaunchDarklyDummyClientImpl

class DummySource(
    application: Application,
    dummyFilePath: String = "."
) : Source {

    private val launchDarklyClient = LaunchDarklyDummyClientImpl(
        application = application,
        dummyFilePath = dummyFilePath
    )
    private val remote = LaunchDarklyRemoteImpl(launchDarklyClient)
    private val repository = FeatureFlagDataRepository(remote)

    override fun getFeatureFlagDataRepository(): FeatureFlagDataRepository = repository
}
