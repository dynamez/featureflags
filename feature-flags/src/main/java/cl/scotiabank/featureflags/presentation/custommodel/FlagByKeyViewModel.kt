package cl.scotiabank.featureflags.presentation.custommodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import cl.scotiabank.featureflags.data.FeatureFlagDataRepository
import cl.scotiabank.featureflags.presentation.di.Source
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

class FlagByKeyViewModel<TFeatureFlag> : ViewModel() {

    fun flagLiveData(featureKey: String, source: Source, tClass: Class<TFeatureFlag>) =
        flagStateFlow(
            featureFlagKey = featureKey,
            tClass = tClass,
            source = source
        ).asLiveData()

    fun flagStateFlow(featureFlagKey: String, tClass: Class<TFeatureFlag>, source: Source) =
        getFeatureFlagByKey(
            featureFlagKey = featureFlagKey,
            tClass = tClass,
            repository = source.getFeatureFlagDataRepository()
        )

    private fun getFeatureFlagByKey(
        featureFlagKey: String,
        tClass: Class<TFeatureFlag>,
        repository: FeatureFlagDataRepository
    ): Flow<TFeatureFlag> = repository
        .getLDValueFlagByKey(featureFlagKey)
        .map { ldValue ->
            parseJsonElement(tClass = tClass, jsonString = ldValue.toJsonString())
        }.flowOn(Dispatchers.IO)

    private fun parseJsonElement(
        tClass: Class<TFeatureFlag>,
        jsonString: String
    ): TFeatureFlag {
        return Gson().fromJson(jsonString, tClass)
    }
}
