# Scotiabank Android Feature Flags
![State](images/kotlin-version.svg)
![State](images/target-sdk.svg)
![State](images/gradle-version.svg)
![State](images/artifactory-version.svg)
![State](images/lifecycle-version.svg)
![State](images/coroutines-version.svg)
![State](images/gson-version.svg)
![State](images/launchdarkly-version.svg)

## 🚧 👷🏗️ Real Time - Work In Progress 🔨👷 🚧

## Description
Feature Flags (or Feature Toggle) is a library for Android which provides flags from Launch Darkly.

This library is focused only on providing Features Flags.

For consuming the flags from Fragments or Activities, Feature Flags library offers 
```LiveData```, ```StateFlow``` and ```Callbacks```. Additionally, if you want to consume it from 
Presenters, Controllers, ViewModels or Processors you can invoke the Repository as well.

<br/>

## Install
In the  root folder ```build.gradle``` file, add the JFrog plugin and the maven repo with the url.
```
allprojects {
    apply plugin: 'com.jfrog.artifactory'
    repositories {
    
        maven { url "http://artifactory.devops.chl.bns/artifactory/remote-bns/" }

    }
}
```
Then you can add the dependency library specifying the version in the app module ```build.gradle``` file.
```
implementation "cl.scotiabank.libraries:feature-flags:2.0.0"
```

<br/>

## How to use it?

We'll cover how to initialize this library and how to use it in your project.

### Initializing

In order to use this library you need to provide an instance. 
It could be either a ```DummySource``` or a ```RemoteSource```.
The way you provide each one of them can vary depending on if you use a dependency tool or manually.

We'll cover some ways with examples in this document.

The ```DummySource``` will require an ```Application``` instance and optionally a file path to load the json file inside 
```assets``` folder. If you don't provide the path, it will look the file in the root of ```assets``` folder.

The ```RemoteSource``` will require ```Application```, ```LDConfig``` and ```LDUser``` instances.
Here is an example of how to build this source:  
```
RemoteSource(
    application = requireActivity().application,
    ldConfig = LDConfig.Builder()
            .stream(false)
            .mobileKey("mob-212as12a-sdg123as-aa14sx-g33a-1233ssa23a2a2gf33")
            .build(),
    ldUser = LDUser.Builder("")
            .custom("appVersion", "4.30.0")
            .custom("os", "android")
            .build()
)
```
**Note:** *If you are using a LaunchDarkly SDK version that is not 3.x.x go to this link
https://docs.launchdarkly.com/sdk/client-side/android/migration-2-to-3 in order to be aware how to provide the
above instances for each LaunchDarkly SDK version (2.x.x or 3.x.x).*

For more details you may find a demo app in this project.

Next, we will show some examples of how to provide the instances using different ways. 
Again, they are just examples. You may use your own specific implementation.

#### Creating instances using Dagger

Create a ```@Module``` for declaring the instances:
```
@Module
abstract class FeatureFlagsModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun createRemoteSource(
            localStorageRepository: LocalStorageRepository,
            application: Application
        ): Source {
            val customerId = localStorageRepository.get().getString(HASHED_ID, "").orEmpty()
            return if (BuildConfig.FLAVOR == "dummy") {
                DummySource(
                    application = application,
                    dummyFilePath = "mock.api/checkversion/launchdarkly.json"
                )
            } else {
                RemoteSource(
                    application = application,
                    ldConfig = LDConfig.Builder()
                        .stream(false)
                        .mobileKey(BuildConfig.LAUNCHDARKLY)
                        .build(),
                    ldUser = LDUser.Builder("")
                        .custom("appVersion", BuildConfig.VERSION_APP)
                        .custom("os", OS_VALUE)
                        .privateCustom("rutcliente", customerId)
                        .build()
                )
            }
        }
    }
}
```
Then, add the above module in your ```ApplicationComponent.java``` and provide the instance as a method:
```
@Singleton
@Component(
    modules = { ApplicationModule.class, FeatureFlagsModule.class }
)
public interface ApplicationComponent {
    ....
    Source getLDSource();
}
```

Finally, declare the ```@Inject``` annotation in your ```Fragment```, ```Activity``` or ```Adapter```:
```
@Inject lateinit var source: Source
```

With this your code is ready for consuming the flags.

### Consuming Flag value

The first step is to create your **own model** in order to parse the data to that model. For instance:
```
// THIS IS JUST AN EXAMPLE. CREATE YOUR OWN MODEL.

import com.google.gson.annotations.SerializedName

data class FeatureFlagsModel(
    @SerializedName(PARAM_ENABLE) val enable: Boolean,
    @SerializedName(PARAM_MESSAGE) val message: String
) {
    companion object {
        const val PARAM_ENABLE = "enable"
        const val PARAM_MESSAGE = "message"
    }
}
```

Then, you should initialize **FlagByKeyViewModel** with the model created in the step above. It could be in your 
Fragment, Activity or wherever you want to use your flag.  

```
private val viewModel by viewModels<FlagByKeyViewModel<FeatureFlagsModel>>()
```

Finally, type your code for observing the ViewModel specifying your **flag key** and the **source variable** declared 
above with Dagger annotation.  
For example, in the code below we are using ```mobile.insurance_declare_claim``` as **flag key**.
```
private fun observeJsonFlag() {
    viewModel.flagStateFlow(
            featureFlagKey = "mobile.insurance_declare_claim",
            tClass = FeatureFlagsModel::class.java,
            source = source
    ).onEach {
        if (it.enable) {
            binding?.btnBoolean?.visibility = View.VISIBLE
        } else {
            binding?.btnBoolean?.visibility = View.GONE
        }
    }.catch { cause ->
        Snackbar.make(binding!!.root, "Error: $cause", Snackbar.LENGTH_LONG).show()
    }.launchIn(lifecycleScope)
}
```
Notice the ```onEach``` function. Here is where you are going to add your own logic.  
We also recommend using the ```.catch``` function in order to manage any exception.

<br/>

## License
```
MIT License

Copyright (c) 2021 Scotiabank - Chile - Android Community

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

