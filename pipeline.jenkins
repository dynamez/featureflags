@Library('pipeline-utils') _

pipeline {
    agent { label 'android-jenkins' }

    options {
        ansiColor('xterm')
        timestamps()
        timeout(time: 30, unit: 'MINUTES')
        disableConcurrentBuilds()
    }

    tools {
        jdk 'Oracle JDK 11'
    }

    environment {
            DEBUG_TYPE = "Debug"
            PROJECT = "feature-flags"
            branchname = "${env.BRANCH_NAME}"
        }

    stages {
        stage('Build App') {
            steps {
                sh './gradlew clean build$DEBUG_TYPE'
            }
        }

        stage('Lint Checks') {
            steps {
                sh './gradlew lint$DEBUG_TYPE'
                androidLint canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '**/build/reports/lint-results*.xml', unHealthy: ''
            }
        }

        stage('Unit Test') {
            steps {
                sh './gradlew -Pcoverage $PROJECT:test$DEBUG_TYPEUnitTest'
            }
            post {
                always {
                    junit '**/build/test-results/**/*.xml'
                }
            }
        }

        stage('Jacoco Code Coverage') {
            steps {
                script {
                    def packageProject = sh(script: "./gradlew -q $PROJECT:getPackage | tail -n 1", returnStdout: true).trim()
                    sh "./gradlew $PROJECT:testDebugUnitTestCoverage"
                    publishHTML target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: "${packageProject}/build/reports/jacoco/testDebugUnitTestCoverage/html",
                        reportFiles: 'index.html',
                        reportName: 'JacocoCov Report'
                    ]
                }
            }
        }


        stage('Code Quality') {
            environment {
                // Variables required by Fortify stage
                branchname = "${env.BRANCH_NAME}"
                projectName = "feature-flags"
                options = "-java-build-dir **/build/classes"
                sourcePath = "feature-flags/src/main/**/*.kt"
                SOURCE_ANALYZER = "${FORTIFY_HOME}/bin/sourceanalyzer"
                REPORT_GENERATOR = "${FORTIFY_HOME}/bin/BIRTReportGenerator"
                REPORTS_DIR = "build/Fortify_Reports"
                FPR_REPORT = "${REPORTS_DIR}/Fortify-SCA-Report_${projectName}.fpr"
                PDF_REPORT = "${REPORTS_DIR}/Fortify-SCA-Report_${projectName}.pdf"
                SONARQUBE_SUFIX = " "
            }
            failFast true
            parallel {
                stage('SonarQube Publish') {
                    steps {
                        script {
                            def sonarqubeName = sh(script: "./gradlew -q ${PROJECT}:getSonarqubeProjectName | tail -n 1", returnStdout: true).trim()
                            def sonarqubeKey = sh(script: "./gradlew -q ${PROJECT}:getSonarqubeProjectKey | tail -n 1", returnStdout: true).trim()
                            if (branchname ==~ /(master|develop)/) {
                                SONARQUBE_SUFIX = "-${branchname}"
                            } else if (branchname ==~ /(PR\-.*)/) {
                                SONARQUBE_SUFIX = "-PR"
                            } else {
                                SONARQUBE_SUFIX = ""
                            }
                            SONAR_KEY = "$sonarqubeKey${SONARQUBE_SUFIX}"
                            SONAR_NAME = "$sonarqubeName${SONARQUBE_SUFIX}"
                            sh "./gradlew ${PROJECT}:sonarqube -Dsonar.host.url=${SONAR_URL} -Dsonar.projectKey=${SONAR_KEY} -Dsonar.projectName=${SONAR_NAME} --stacktrace"
                        }
                    }
                }
                //stage('Fortify Static Code Analysis') {
                //    when {
                //        environment name: 'branchname', value: 'master'
                //    }
                //    steps {
                //        script {
                //            codeQuality.fortifyStage(options, sourcePath)
                //        }
                //    }
                //}
                stage('Blackduck') {
                    when {
                        environment name: 'branchname', value: 'master'
                    }
                    steps {
                        script{
                            podSlaves.blackduckStage()
                        }
                    }
                }
            }
        }

        stage('Assemble Library') {
            when {
                environment name: 'branchname', value: 'master'
            }
            steps {
                sh './gradlew assembleRelease'
            }
        }

        stage('Publish') {
            when {
                environment name: 'branchname', value: 'master'
            }
            environment {
                ARTIFACTORY = credentials('Artifactory')
                ARTIFACTORYREPOKEY = 'gradle-sgo-android-modules'
            }
            steps {
                sh './gradlew artifactoryPublish'
            }
        }
    }
    post {
        always {
            echo 'Cleaning workspace...'
            deleteDir()
            //slack()
        }
    }
}

def slack() {
    def channel = '#cl-sgo-android-blueprint-pr'
    def result = currentBuild.result ?: 'SUCCESS' // for jenkins result null means SUCCESS
    if (result in ['FAILURE']) { // will notify only on these results
        def color = [FAILURE: 'danger', SUCCESS: 'good'].withDefault({ 'warning' })
        slackSend channel: channel, color: color[result], \
        message: "<${BUILD_URL}|Build ${currentBuild.fullDisplayName} Finished: ${result}>"
    }
}
