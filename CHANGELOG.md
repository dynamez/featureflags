# Changelog
All notable changes made for this library

## [2.0.0](https://artifactory.cldevops.chl.bns/artifactory/webapp/#/artifacts/browse/tree/General/gradle-sgo-android-modules/cl/scotiabank/libraries/feature-flags/2.0.0) (2021-10-13)

### Breaking Changes
- LaunchDarkly library upgrade to 3.1.1. This will change the way to provide LDClient and LDUser.
- Refactoring based on 3.1.1 version changes.
- Upgrading to Target SDK 31.

### Improvements
- Upgrading Coroutines, Kotlin, JFrog and AGP version.
- Changing Java version in Pipeline to use Oracle JDK 11.
- Adding quality tools: Sonarqube, JaCoCo. Enabling it in pipeline.
- Removing domain package.

## [1.0.1](https://artifactory.cldevops.chl.bns/artifactory/webapp/#/artifacts/browse/tree/General/gradle-sgo-android-modules/cl/scotiabank/libraries/feature-flags/1.1.0) (2021-05-25)

### Improvements
* Removing catch in ```getFeatureFlagByKey``` method in ```FlagByKeyViewModel```. 
* Removing unused methods in ```LaunchDarklyClientImpl```.
* Method ```getJsonFlag``` in ```LaunchDarklyDummyClientImpl``` now returns a valid JsonElement from the dummy source.
* Removing ```CUSTOMER_ID``` constant.

## [1.0.0](https://artifactory.cldevops.chl.bns/artifactory/webapp/#/artifacts/browse/tree/General/gradle-sgo-android-modules/cl/scotiabank/libraries/feature-flags/1.0.0) (2021-04-07)

### Features
- Target SDK 30
- Get flag by keys based on a custom Model.
- Get flag by keys as a raw data.
- Launch Darkly integration.