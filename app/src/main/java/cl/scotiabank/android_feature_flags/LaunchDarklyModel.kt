package cl.scotiabank.android_feature_flags

import com.google.gson.annotations.SerializedName

data class LaunchDarklyModel(
    @SerializedName("enable") val enable: Boolean,
    @SerializedName("message") val message: String
)