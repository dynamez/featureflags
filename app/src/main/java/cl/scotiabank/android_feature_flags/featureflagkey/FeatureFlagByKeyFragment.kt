package cl.scotiabank.android_feature_flags.featureflagkey

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import cl.scotiabank.android_feature_flags.LaunchDarklyModel
import cl.scotiabank.android_feature_flags.R
import cl.scotiabank.android_feature_flags.databinding.FragmentFeatureFlagByKeyBinding
import cl.scotiabank.featureflags.presentation.custommodel.FlagByKeyViewModel
import cl.scotiabank.featureflags.presentation.di.DummySource
import cl.scotiabank.featureflags.presentation.di.RemoteSource
import cl.scotiabank.featureflags.presentation.di.Source
import com.google.android.material.snackbar.Snackbar
import com.launchdarkly.sdk.LDUser
import com.launchdarkly.sdk.android.LDConfig
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FeatureFlagByKeyFragment : Fragment() {

    private var binding: FragmentFeatureFlagByKeyBinding? = null
    private val viewModel by viewModels<FlagByKeyViewModel<LaunchDarklyModel>>()
    private lateinit var source: Source

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        if (binding == null) {
            binding = FragmentFeatureFlagByKeyBinding.inflate(inflater, container, false)
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val runnable = Runnable {
            activity?.runOnUiThread {
                source = createDummySource()
                observeJsonFlag()
            }
        }
        Handler(Looper.getMainLooper()).postDelayed(runnable, 300)
    }

    private fun observeJsonFlag() {
        viewModel.flagStateFlow(
                featureFlagKey = "mobile.insurance_declare_claim",
                tClass = LaunchDarklyModel::class.java,
                source = source
        ).onEach {
            setJsonText(it)
            setBooleanButton(it)
        }.catch { cause ->
            Snackbar.make(binding!!.root, "Error: $cause", Snackbar.LENGTH_LONG).show()
        }.launchIn(lifecycleScope)
    }

    private fun createRemoteSource() = RemoteSource(
            application = requireActivity().application,
            ldConfig = LDConfig.Builder()
                    .stream(false)
                    .mobileKey("mob-489028fc-15a2-4c0f-b00b-20c3e8f8879c")
                    .build(),
            ldUser = LDUser.Builder("")
                    .custom("appVersion", "4.30.0")
                    .custom("os", "android")
                    .privateCustom("rutcliente", "5feceb66ffc86f38d952786c6d696c79c2dbc239dd4e91b46729d73a27fb57e9")
                    .build()
    )

    private fun createDummySource() = DummySource(
        application = requireActivity().application,
        dummyFilePath = "checkversion/launchdarkly.json"
    )

    private fun setJsonText(ldModel: LaunchDarklyModel) {
        val jsonText = "${requireContext().getString(R.string.json_feature_flag_value)}: $ldModel"
        binding?.textView?.text = jsonText
    }

    private fun setBooleanButton(ldModel: LaunchDarklyModel) {
        if (ldModel.enable) {
            binding?.btnBoolean?.visibility = View.VISIBLE
        } else {
            binding?.btnBoolean?.visibility = View.GONE
        }
    }

}