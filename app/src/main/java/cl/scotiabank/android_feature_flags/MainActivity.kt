package cl.scotiabank.android_feature_flags

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import cl.scotiabank.android_feature_flags.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null
    private lateinit var appBarConfiguration: AppBarConfiguration
//    private val allFeaturesViewModel by viewModels<AllFeaturesViewModel<LaunchDarklyModel>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (binding == null) {
            binding = ActivityMainBinding.inflate(layoutInflater)
        }
        setContentView(binding?.root)
        setupToolbar()
        val navHostFragment = getNavHostFragment()
        appBarConfiguration = AppBarConfiguration(navHostFragment.navController.graph)
        setupActionBarWithNavController(navHostFragment.navController, appBarConfiguration)

//        allFeaturesViewModel.source = DummySource(
//                application = application,
//                dummyFilePath = "checkversion/launchdarkly.json"
//        )
//        observeFeatureViewModel()
//        verifyFeatureToggles()
    }

    private fun setupToolbar() {
        setSupportActionBar(binding?.toolbarMain)
    }

    private fun getNavHostFragment(): NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.main_nav_host_fragment) as NavHostFragment

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

//    private fun verifyFeatureToggles() {
//        allFeaturesViewModel.getAllUsingKotlinFlow(tClass = LaunchDarklyModel::class.java)
//    }
//
//    private fun observeFeatureViewModel() {
//        allFeaturesViewModel
//            .stateFlow()
//            .filterNot { it.isEmpty() }
//            .onEach { launchDarkly ->
//                if (launchDarkly["mobile.menu_medallia"]?.enable == true) {
//                    binding?.button?.visibility = View.VISIBLE
//                } else {
//                    binding?.button?.visibility = View.GONE
//                }
//            }
//            .launchIn(lifecycleScope)
//    }

}