package cl.scotiabank.android_feature_flags.index

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cl.scotiabank.android_feature_flags.MainNavigation
import cl.scotiabank.android_feature_flags.databinding.FragmentMainBinding
import cl.scotiabank.android_feature_flags.index.model.IndexItemFactory

class MainFragment : Fragment() {

    private var binding: FragmentMainBinding? = null
    private val indexItemAdapter = IndexItemAdapter()
    private val mainNavigation = MainNavigation()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        if (binding == null) {
            binding = FragmentMainBinding.inflate(inflater, container, false)
        }
        mainNavigation.setupView(binding?.root)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        binding?.rvIndex?.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = indexItemAdapter
        }
        indexItemAdapter.setData(IndexItemFactory.makeDefaultList(
                context = requireContext(),
                mainNavigation = mainNavigation
        ))
    }

}