package cl.scotiabank.android_feature_flags.index

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.scotiabank.android_feature_flags.index.model.IndexItem

class IndexItemAdapter : RecyclerView.Adapter<IndexItemViewHolder>() {

    var indexItems: List<IndexItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            IndexItemViewHolder.create(parent)

    override fun onBindViewHolder(holder: IndexItemViewHolder, position: Int) {
        holder.bind(
            label = indexItems[position].label,
            event = indexItems[position].event
        )
    }

    override fun getItemCount(): Int = indexItems.count()

    fun setData(newIndexItems: List<IndexItem>) {
        indexItems = newIndexItems
        notifyDataSetChanged()
    }

}