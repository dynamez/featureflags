package cl.scotiabank.android_feature_flags.index

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.scotiabank.android_feature_flags.databinding.ViewIndexItemBinding

class IndexItemViewHolder(private val binding: ViewIndexItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

        fun bind(label: String, event: () -> Unit) {
            binding.apply {
                tvItemText.text = label
                tvItemText.setOnClickListener { event() }
            }
        }

    companion object {
        fun create(parent: ViewGroup): IndexItemViewHolder {
            val view = ViewIndexItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
            )
            return IndexItemViewHolder(view)
        }
    }

}