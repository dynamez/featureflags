package cl.scotiabank.android_feature_flags.index.model

import android.content.Context
import cl.scotiabank.android_feature_flags.MainNavigation
import cl.scotiabank.android_feature_flags.R

data class IndexItem(
    val label: String,
    val event: () -> Unit
)

object IndexItemFactory {

    fun makeDefaultList(context: Context, mainNavigation: MainNavigation) = arrayListOf(
        IndexItem(
            label = context.getString(R.string.all_feature_flags),
            event = { mainNavigation.navigateToAllFeatureFlagsScreen() }
        ),
        IndexItem(
            label = context.getString(R.string.feature_flag_by_key),
            event = { mainNavigation.navigateToFeatureFlagByKeyFragment() }
        ),
    )

}