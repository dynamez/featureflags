package cl.scotiabank.android_feature_flags

import android.view.View
import androidx.navigation.findNavController

class MainNavigation {

    private var view: View? = null

    fun setupView(view: View?) {
        this.view = view
    }

    fun navigateToAllFeatureFlagsScreen() {
        view?.findNavController()?.navigate(R.id.action_main_to_allFeatureFlags)
    }

    fun navigateToFeatureFlagByKeyFragment() {
        view?.findNavController()?.navigate(R.id.action_main_to_featureFlagByKey)
    }

}